'use strict';

var preferences = {
    lemmingWalkingSpeed: 50,
};

var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 100 },
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
var cursorKeys = null;      // pulsanti premuti
var lemmings = null;        // lista lemming
var terrain = null;


// caricamento asset
function preload() {
    // sfondo
    this.load.image('sky', 'assets/sky.png');
    // terreno
    this.load.image('terrain', 'assets/terrain-tile.png');
    // lemming
    this.load.spritesheet('lemming',
        'assets/lemmings.png',
        { frameWidth: 37, frameHeight: 43 }
    );
    this.load.spritesheet('lemming-walking-dx',
        'assets/lemming-walking-dx.png',
        { frameWidth: 37, frameHeight: 43 }
    );
    this.load.spritesheet('lemming-walking-sx',
        'assets/lemming-walking-sx.png',
        { frameWidth: 37, frameHeight: 43 }
    );
    this.load.spritesheet('lemming-stopping',
        'assets/lemming-stopping.png',
        { frameWidth: 50, frameHeight: 44 }
    );
}


// creazione scena
function create() {
    this.physics.world.setBounds(0, 0, config.width, config.height, true, true, true, true);

    // sfondo
    this.add.image(0, 0, 'sky').setOrigin(0, 0);

    // terreno
    terrain = this.physics.add.staticGroup();
    createTerrain();

    // animazioni lemming
    this.anims.create({
        key: 'lemming-halt',
        frames: [ { key: 'lemming', frame: 0 } ],
        frameRate: 10
    });
    this.anims.create({
        key: 'lemming-walking-dx',
        frames: this.anims.generateFrameNumbers('lemming-walking-dx', { start: 0, end: 6 }),
        frameRate: 10,
        repeat: -1
    });
    this.anims.create({
        key: 'lemming-walking-sx',
        frames: this.anims.generateFrameNumbers('lemming-walking-sx', { start: 0, end: 6 }),
        frameRate: 10,
        repeat: -1
    });
    this.anims.create({
        key: 'lemming-stopping',
        frames: this.anims.generateFrameNumbers('lemming-stopping', { start: 5, end: 15 }),
        frameRate: 10,
        repeat: -1
    });

    // lista lemming
    lemmings = this.physics.add.group();
    this.physics.add.collider(lemmings, terrain);
    this.physics.add.overlap(lemmings, lemmings, manageLemmingsOverlap, null, this);

    cursorKeys = this.input.keyboard.createCursorKeys();

    // begin game
    startLemmingsGenerator();
}


// eseguita ad ogni ciclo
function update() {
    // pressione frecce
    // if (cursorKeys.left.isDown) {
    //     l.x -= 10;
    //     console.log('freccia <--');
    // } else if (cursorKeys.right.isDown) {
    //     l.x += 10;
    //     console.log('freccia -->');
    // }

    this.input.on('gameobjectdown', onObjectClicked);

    // makes each lemming act
    lemmingActivity();
}




/**
 * Crea un terreno di test
 */
function createTerrain() {
    for (var y = 0; y < config.height; y+=5) {
        for (var x = 0; x < config.width; x+=5) {
            if (y > config.height/3*2) {
                terrain.create(x, y, 'terrain');
            }
        }
    }
}


/**
 * Manages lemmings activity
 */
function lemmingActivity() {
    if (lemmings) {
        lemmings.children.iterate(function(lemming) {
            // falling
            if (!lemming.body.touching.down) {
                lemming._info.currentAction = 'falling';
            } else {
                // se non stava facendo nulla, inizia a camminare
                if (lemming._info.currentAction === null) {
                    lemming._info.currentAction = 'walking-dx';
                    lemming.anims.play('lemming-walking-dx', true);
                    lemming.setVelocityX(preferences.lemmingWalkingSpeed);
                } else if (lemming._info.currentAction === 'falling') {
                    // se stava cadendo, vedi se muore spiaccicato o se inizia a camminare
                    lemming._info.currentAction = 'walking-dx';
                    lemming.anims.play('lemming-walking-dx', true);
                    lemming.setVelocityX(preferences.lemmingWalkingSpeed);
                } else if (lemming._info.currentAction === 'walking-dx') {
                    // se stava camminando e raggiunge qualcosa a dx
                    if (lemming.body.touching.right || lemming.x >= config.width - lemming.body.width/2) {
                        lemmingInvertWalking(lemming);
                        // lemming._info.currentAction = 'walking-sx';
                        // lemming.anims.play('lemming-walking-sx', true);
                        // lemming.setVelocityX(-1 * preferences.lemmingWalkingSpeed);
                    }
                } else if (lemming._info.currentAction === 'walking-sx') {
                    // se stava camminando e raggiunge qualcosa a sx
                    if (lemming.body.touching.left || lemming.x <= 5 + lemming.body.width/2) {
                        lemmingInvertWalking(lemming);
                        // lemming._info.currentAction = 'walking-dx';
                        // lemming.anims.play('lemming-walking-dx', true);
                        // lemming.setVelocityX(preferences.lemmingWalkingSpeed);
                    }
                } else if (lemming._info.currentAction === 'stopping') {
                    // ste sta fermando gli altri lemming
                    lemming.anims.play('lemming-stopping', true);
                    lemming.setVelocityX(0);
                }
            }
        });
    }
}


/**
 * Gestisce quando due lemming entrano in contatto
 */
function manageLemmingsOverlap(lemming1, lemming2) {
    // se uno è un vigile, inverte la velocità dell'altro, se sta camminando
    if (lemming1._info.currentAction === 'stopping' || lemming2._info.currentAction === 'stopping') {
        if (lemming1._info.currentAction !== 'stopping') {
            lemmingInvertWalking(lemming1);
        } else {
            lemmingInvertWalking(lemming2);
        }
    }
}


/**
 * Inverte la direzine della camminata di un lemming
 * @param lemming
 */
function lemmingInvertWalking(lemming) {
    if (lemming._info.currentAction === 'walking-dx') {
        lemming._info.currentAction = 'walking-sx';
        lemming.anims.play('lemming-walking-sx', true);
        lemming.setVelocityX(-1 * preferences.lemmingWalkingSpeed);
    } else {
        lemming._info.currentAction = 'walking-dx';
        lemming.anims.play('lemming-walking-dx', true);
        lemming.setVelocityX(preferences.lemmingWalkingSpeed);
    }
}




/**
 * Generating lemmings
 */
function startLemmingsGenerator() {
    var generatedLemmings = 1;
    spawnLemming();
    var lemmingsGenerator = setInterval(function() {
        spawnLemming();
        generatedLemmings++;
        if (generatedLemmings > 5) {
            clearInterval(lemmingsGenerator);
            console.log(lemmings);
        }
    }, 2000);
}




/**
 * Spawns a single lemming
 */
function spawnLemming() {
    lemmings.create(config.width/2, config.height/2, 'lemming');
    lemmings.children.iterate(function(lemming) {
        lemming.anims.play('lemming-halt', true);
        lemming.body.setCollideWorldBounds(true);
        lemming.onWorldBounds = true;
        lemming.body.onWorldBounds = true;
        lemming.setInteractive();
        // information structure
        lemming._info = {
            objectType: 'lemming',
            currentAction: null,
        };
    });
}




/**
 * Quando clicco su un oggetto
 *
 * @param pointer
 * @param gameObject
 */
function onObjectClicked(pointer, gameObject) {
    // console.log('onObjectClicked()');
    // console.log(pointer);
    // console.log(gameObject);
    // se ho cliccato su un lemming
    if (gameObject && gameObject._info && gameObject._info.objectType === 'lemming') {
        gameObject._info.currentAction = 'stopping';
    }
}
